#!/bin/bash

while true
do
    inotifywait -e modify /traefik/acme.json
    echo "`date +%y/%m/%d_%H:%M:%S`:: /traefik/acme.json modified"
    traefik-certs-dumper file --source /traefik/acme.json --dest /traefik/ssl/
    ln -f /traefik/ssl/certs/* /traefik/ssl/
    ln -f /traefik/ssl/private/* /traefik/ssl/
    pushd /traefik/ssl > /dev/null
        set -f
        for cert in $(find ./ -name '\*.*.crt' -maxdepth 1)
        do
            echo "Found wildcard certificate: $cert"
            DOMAIN=$( echo $(basename $cert) | sed 's/*.//g' | sed 's/.crt$//g' )
            echo "Read domain from wildcard certificate: $DOMAIN"
            rm -rf /traefik/ssl/mail.${DOMAIN}
            mkdir /traefik/ssl/mail.${DOMAIN}
            cp /traefik/ssl/\*.${DOMAIN}.crt /traefik/ssl/mail.${DOMAIN}/fullchain.pem
            cp /traefik/ssl/\*.${DOMAIN}.key /traefik/ssl/mail.${DOMAIN}/key.pem
            openssl x509 -in /traefik/ssl/mail.${DOMAIN}/fullchain.pem -out /traefik/ssl/mail.${DOMAIN}/cert.pem
            set +f
            chmod 644 /traefik/ssl/mail.${DOMAIN}/*
            set -f
        done
        set +f
    popd > /dev/null
done
