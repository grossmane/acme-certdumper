FROM  ldez/traefik-certs-dumper 
MAINTAINER Jens Grossmann <grossmane@users.noreply.github.com>

RUN apk --no-cache add inotify-tools jq openssl util-linux bash && \
        mkdir -p /traefik/ssl/

ADD watch.sh /watch.sh

RUN chmod +x /watch.sh

ENTRYPOINT ["/watch.sh"]
